// Copyright © 2022-2023 Massimo Gismondi

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>

#[macro_use] extern crate lazy_static;
use gtk::prelude::*;
use gtk::{Label, Button};
use gtk::{Application, ApplicationWindow};
use gtk::{Orientation};
use gtk::gdk_pixbuf::Pixbuf;
use gio;
use std::sync::Mutex;

use airone::prelude::*;
use airone::airone_derive;
airone_init!();
#[derive(airone_derive::AironeDbDerive)]
#[derive(Debug)]
struct MyData
{
    field_a: i32,
    field_b: i32
}

lazy_static!(
    static ref DB: Mutex<AironeDb<MyData>> = Mutex::new(AironeDb::new());
);

// Name it gtk::Box instead of Box
// to not collide with Rust's internal Box struct
fn form_add_substract(element_to_change: usize) -> gtk::Box
{
    let b = gtk::Box::new(Orientation::Horizontal, 5);

    {
        // Field A
        let label = Label::builder().label(
            &format!("Field A: {}", DB.lock().unwrap().get_field_a(element_to_change))
            ).build();
        {
            let label_clone = label.clone();
            let button_decrement = Button::builder().label("-1").build();
            button_decrement.connect_clicked(move |_b|
            {
                let mut db = DB.lock().unwrap();
                let prev_value = *db.get_field_a(element_to_change);
                let new_value = prev_value-1;
                db.set_field_a(element_to_change, new_value);
                label_clone.set_label(
                    &format!("Field A: {}", new_value)
                )
            });
            b.add(&button_decrement);
        }
        {
            let label_clone = label.clone();
            let button_increment = Button::builder().label("+1").build();
            button_increment.connect_clicked(move |_b|
            {
                let mut db = DB.lock().unwrap();
                let prev_value = *db.get_field_a(element_to_change);
                let new_value = prev_value+1;
                db.set_field_a(element_to_change, new_value);
                label_clone.set_label(
                    &format!("Field A: {}", new_value)
                )
            });
            b.add(&button_increment);
        }
        b.add(&label);
    }
    b.add(&gtk::Separator::new(Orientation::Vertical));
    {
        // Field B
        let label = Label::builder().label(
            &format!("Field B: {}", DB.lock().unwrap().get_field_b(element_to_change))
            ).build();
        {
            let label_clone = label.clone();
            let button_decrement = Button::builder().label("-1").build();
            button_decrement.connect_clicked(move |_b|
            {
                let mut db = DB.lock().unwrap();
                let prev_value = *db.get_field_b(element_to_change);
                let new_value = prev_value-1;
                db.set_field_b(element_to_change, new_value);
                label_clone.set_label(
                    &format!("Field B: {}", new_value)
                )
            });
            b.add(&button_decrement);
        }
        {
            let label_clone = label.clone();
            let button_increment = Button::builder().label("+1").build();
            button_increment.connect_clicked(move |_b|
            {
                let mut db = DB.lock().unwrap();
                let prev_value = *db.get_field_b(element_to_change);
                let new_value = prev_value+1;
                db.set_field_b(element_to_change, new_value);
                label_clone.set_label(
                    &format!("Field B: {}", new_value)
                )
            });
            b.add(&button_increment);
        }
        b.add(&label);
    
    }
    
    
    return b;
}


fn main() {
    gio::resources_register_include!("compiled.gresource").unwrap();

    let app = Application::builder()
        .application_id("org.example.HelloWorld")
        .build();
    
    {
        let mut db = DB.lock().unwrap();
        while db.len() < 2
        {
            db.push(MyData { field_a: 0, field_b: 0 });
        }
    }

    app.connect_activate(|app| {
        let win = ApplicationWindow::builder()
            .application(app)
            .default_width(320)
            .default_height(200)
            .title("Hello, World!")
            .icon(
                    &Pixbuf::from_resource(
                    "/it/yourcompany/helloworld/icon.png"
                ).unwrap()
            )
            .build();

        let cont = gtk::Box::new(Orientation::Vertical, 4);
        cont.set_margin(8);
        win.add(&cont);
        cont.add(&Label::builder().label("Object 0").build());
        cont.add(&form_add_substract(0));
        cont.add(&gtk::Separator::new(Orientation::Horizontal));
        cont.add(&Label::builder().label("Object 1").build());
        cont.add(&form_add_substract(1));

        win.show_all();
    });

    app.run();
}